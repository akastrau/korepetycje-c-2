#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <ctype.h>
#include <string.h>
#include "ukladanka.h"
#include <time.h>

int main()
{
	srand(time(NULL));
	int i, j= 0;
	char read;
	char nick[35];
	
	GameData data = { 0,.tab = {
		{ 1,0,2 },//Test tab
		{ 3,4,5 },
		{ 6,7,8 } } };

	Winner dane[3] = { 0 };

	printf("####################\nWITAJ W UKLADANKA SIMULATOR 2000!\n####################\tby Kopssosoft\n");
	menu:;
	printf("1.Nowa gra.\n");
	printf("2.Wczytaj gre.\n");
	printf("3.Najlepsi gracze.\n");
	printf("4.Instrukcja.\n");

	switch (_getch())
	{

	case '2':
		if (load(&data) != 1)
		{
			system("cls");
			printf("Witaj %s !\n", data.nick);
			break;
		}		
		
	case '1':
		
		printf("Podaj nick: ");
		scanf_s("%s", nick, sizeof(nick));
		random(&data);
		data.score = 0;
		strcpy_s(data.nick, strlen(nick) + 1, nick);
		break;
	case '3':
		if (prepareWinnerFile(dane) == 1 || dane[0].score == 0)
		{
			printf("Nie ma zadnych wynikow!\n");
			_getch();
			system("cls");
			goto menu;
		}
		showWinners(dane);
		_getch();
		system("cls");
	
	case '4':
		instruction();
		

	default:
		goto menu;
		break;	
	}
	
		showtab(data);		
		win(data,0,nick,dane);
		position(data, &i, &j);
		
		while (1)
	{
			read = _getch();
			char choice = (tolower(read));
			
			switch (choice)
			{
			case 'a'://left
				position(data, &i, &j);
				left(&data, i, j);
				system("cls");
				showtab(data);
				data.score++;
				win(data, data.score,nick,dane);
				break;
			
			case 'd'://right
				position(data, &i, &j);
				right(&data, i, j);
				system("cls");
				showtab(data);
				data.score++;
				win(data,data.score,nick, dane);
				break;

			case 'w'://up
				position(data, &i, &j);
				up(&data, i, j);
				system("cls");
				showtab(data);
				data.score++;
				win(data,data.score,nick,dane);
				break;
			
			case 's'://down
				position(data, &i, &j);
				down(&data, i, j);
				system("cls");
				showtab(data);
				data.score++;
				win(data,data.score,nick,dane);
				break;
	
			case 'r'://random
				data.score = 0;
				system("cls");
				random(&data);
				showtab(data);
				break;
		
			case 'q'://escape
				save(data);
				exit(0);
				break;
			
			default:
				break;
			}
		}	
	getchar();
	return 0;
}
