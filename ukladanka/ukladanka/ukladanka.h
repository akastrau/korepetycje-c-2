#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <ctype.h>
#include <time.h>
typedef struct GameData
{
	int score;
	char nick[35];
	int tab[3][3];
}GameData;

typedef struct Winner
{
	int counter;
	int score;
	char nick[35];
}Winner;

void instruction();

int load(GameData *data);

void save(GameData data);

void random(GameData *data);

void left(GameData *data, int i, int j);

void right(GameData *data, int i, int j);

void up(GameData *data, int i, int j);

void down(GameData *data, int i, int j);

void position(GameData data, int* row, int* column);

int win(GameData data, int movements, char nick[35], Winner *dane);

void showtab(GameData data);

int prepareWinnerFile(Winner *data);

int saveWinners(Winner *data, GameData gameData);

void showWinners(const Winner *data);