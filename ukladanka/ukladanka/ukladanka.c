#include "ukladanka.h"
#include <string.h>

void instruction()
{
	system("cls");
	printf("\nWitaj w Ukladanka Symulator 2000!\n");
	printf("\nCelem gry jest uloznie liczb w porzadku rosnacym\n");
	printf("WSAD - poruszanie sie po planszy\n");
	printf("R - reset gry\n");
	printf("Q - opuszczenie gry oraz jej zapisanie\n");
	_getch();
	system("cls");
}


int load(GameData *data)
{
	FILE *file = NULL;
	errno_t error = fopen_s(&file,	"gamedata.bin", "rb");
	if (error != 0) {
		printf("Brak stanu gry do wczytania.\n");
		return 1;
	}
	else {
		fread(data, sizeof(struct GameData), 1, file);
		fclose(file);
		return 0;
	}
}

void save(GameData data) 
{
	FILE *file = NULL;
	errno_t error = fopen_s(&file, "gamedata.bin", "wb");
	fwrite(&data, sizeof(struct GameData), 1, file);
	fclose(file);
}


void random(GameData *data)
{
	int pool[9] = { 0 };
	int c = 0;
	int r,x;
	int i = 0;
	while (i<9) 
	{
		r = rand() % 9;
		for (x = 0; x < i; x++)
		{
			if (pool[x] == r) 
			{
				break;
			}
		}
		if (x == i) 
		{
			pool[i++] = r;
		}
	}
	//pool[r] = 0;

	for (size_t a = 0; a < 3; a++)
	{
		for (size_t b = 0; b < 3; b++)
		{
			data->tab[a][b] = pool[c];
			c++;
		}
	}

}
void left(GameData *data, int i, int j)
{
	if (j - 1 >= 0)
	{
		data->tab[i][j]=data->tab[i][j - 1];
		data->tab[i][j - 1] = 0;
	}
}
void right(GameData *data, int i, int j)
{
	if (j + 1 <= 2)
	{
		data->tab[i][j]= data->tab[i][j + 1];
		data->tab[i][j + 1] = 0;
	}
}
void up(GameData *data, int i, int j)
{
	if (i - 1 >= 0)
	{
		data->tab[i][j]= data->tab[i - 1][j];
		data->tab[i - 1][j] = 0;
	}
}
void down(GameData *data, int i, int j)
{
	if (i + 1 <= 2)
	{
		data->tab[i][j] = data->tab[i + 1][j];
		data->tab[i + 1][j] = 0;
	}
}

void position(GameData data, int* row, int* column)
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (data.tab[i][j] == 0)
			{
				*row = i;
				*column = j;
			}
		}
	}
}

int win(GameData data, int movements, char nick[35], Winner *dane)
{
	int counter = 0;
	for (size_t i = 0; i < 3; i++)
	{
		for (size_t j = 0; j < 2; j++)
		{
			if ((data.tab[i][j] < data.tab[i][j + 1]) && (data.tab[0][2] < data.tab[1][0]) && (data.tab[1][2] < data.tab[2][0]))
			{
				counter++;
				if (counter == 6)
				{
					printf("Wygrales! Liczba ruchow: %i\n", movements);

					if (saveWinners(dane, data) == 0)
					{
						printf("Gratulacje! Dostales sie do galerii chwal!\n");
					}

					printf("Press something to exit...");
					_getch();

					exit(0);
				}
			}
			
		}
	}
	return 0;
}

void showtab(GameData data)
{
	for (size_t i = 0; i<3; i++)
	{
		for (size_t j = 0; j < 3; j++)
		{
			if(data.tab[i][j] !=0) printf("|%i| ", data.tab[i][j]);
			else printf("| | ");
		}
		printf("\n");
	}
	printf("\n");
}

int prepareWinnerFile(Winner *data)
{
	// Sprawdz czy plik istnieje
	// jesli nie, utworz go!
	FILE *file;
	errno_t error = fopen_s(&file, "winner.bin", "rb");
	if (error) {
		error = fopen_s(&file, "winner.bin", "wb");
		fwrite(data, sizeof(Winner), 3, file); // zapisz 3 graczy (puste struktury)
		fclose(file);
		return 1; // Nie ma na razie zadnych wynikow
	}
	else {
		// Wczytaj aktualny plik
		fread(data, sizeof(struct Winner), 3, file);
		fclose(file);
	}
	return 0;
}

int saveWinners(Winner *data, GameData gameData) 
{
	// Szukaj pustego miejsca lub miejsca na lepszy score
	for (size_t i = 0; i < 3; i++)
	{
		if (data[i].score > gameData.score || data[i].score == 0) 
		{
			// Znaleziono puste miejsce!!
			data[i].score = gameData.score;
			strcpy_s(data[i].nick, strlen(gameData.nick) + 1, gameData.nick);

			FILE *file;
			errno_t error = fopen_s(&file, "winner.bin", "wb");
			fwrite(data, sizeof(struct Winner), 3, file);
			fclose(file);
			return 0;
		}
	}
	// Nie byles lepszy lamerze!
	return 1;
}

void showWinners(const Winner *data)
{
	printf("Tablica chwaly... czyli the best of the best!\n\n");

	for (size_t i = 0; i < 3; i++)
	{
		if (data[i].score != 0) 
		{
			printf("\t\t%s - %i\n\n", data[i].nick, data[i].score);
		}
		
	}
}
