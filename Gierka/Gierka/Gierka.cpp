#include "Gierka.h"

bool collision(Player &player, Coin& coin, bool sound, int &counter)
{
	if (player.x + coin.width<coin.xc || player.x>coin.xc + coin.width || player.y + coin.height <coin.yc || player.y>coin.yc + coin.height)
	{
		//NO COLLISION
		return false;
	}
	else
	{
		if (player.level >= 0)
		{
			coin.xc = rand() % (ScreenWidth - 23);
			coin.yc = rand() % 210 + 30;
		}
		if (player.level >= 1)
		{
			coin.horizontalMovement = rand() % 2;
			coin.verticalMovement = rand() % 2;
		}
		if (sound)
			al_play_sample(coin.soundEffect, 1, 0, 3, ALLEGRO_PLAYMODE_ONCE, 0);
		if (coin.timeCoin)
		{
			counter++;
			player.sec += 10;
		}
		if(coin.slowdownCoin)
		{
			counter++;
		}
		else
			player.score++;
		
		return true;
	}
}
/*Player p2[2];
	p2[0] = {0,0,314,159,0,0,1,2,3};
	p2[1] = { 0,0,111,222,0,0,33,33,33 };
	ofstream plikout("gamedata.bin", ios::binary | ios::app);
	plikout.write((char*)&p2, sizeof(p2));
	plikout.close();

	ifstream plikin("gamedata.bin", ios::binary);
	Player p[2];
	plikin.read((char*)&p, sizeof(p));
	for (int i = 0; i < 2; i++)
	{
		cout << "Level: " << p[i].level << endl;
		cout << "X: " << p[i].x << endl;
		cout << "Y: " << p[i].y << endl;
		cout << "Score: " << p[i].score << endl;
		cout << "Sec: " << p[i].sec << endl;
	}*/