#include <stdio.h>
#include <stdlib.h>

typedef struct GameData
{
	int score;
	char playerName[35];
	const int playerID;
} GameData;

int main() {
	FILE *file = NULL;
	errno_t error = fopen_s(&file, "gamedata.bin", "rb");
	int playersCount = 0;

	fread(&playersCount, sizeof(int), 1, file);
	GameData *data = (GameData*)malloc(playersCount * sizeof(struct GameData));
	fread(data, sizeof(struct GameData), playersCount, file);
	fclose(file);

	printf("Game data (format JSON)\n {\n");

	for (int i = 0; i < playersCount; ++i)
	{
		printf("\t\t{score: %i, playerName: \"%s\", playerID: %i},\n", data[i].score, data[i].playerName, data[i].playerID);
	}
	printf("}\n");

	free(data);

	getchar();
	return 0;
}