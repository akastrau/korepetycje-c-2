#include <stdio.h>
#include <stdlib.h>

typedef struct myStruct {
	int age;
	char* name;
	float mark;

} myStruct, *ptrMyStruct;

int main()
{
	//myStruct data[10];
	ptrMyStruct anotherDataAsPtr = (myStruct*)malloc(sizeof(myStruct));
	anotherDataAsPtr->mark = 10;
	
	printf("%f\n", anotherDataAsPtr->mark);
	// Do something cool :)

	free(anotherDataAsPtr);
	system("pause");
	return 0;
}