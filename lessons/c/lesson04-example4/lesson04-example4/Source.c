#include <stdio.h>
#include <stdlib.h>

void fillMatrix(int **matrix, int nRows, int nColumns)
{
	for (int i = 0; i < nRows; i++)
	{
		for (int j = 0; j < nColumns; j++)
		{
			matrix[i][j] = i + j;
		}
	}
}

void printMatrix(int **matrix, int nRows, int nColumns)
{
	for (int i = 0; i < nRows; i++)
	{
		for (int j = 0; j < nColumns; j++)
		{
			printf("%i ", matrix[i][j]);
		}
	}
	printf("\n");
}

int main()
{
	int **matrix = NULL;
	int nRows, nColumns;
	printf("Enter the dim of matrix: ");
	scanf_s("%i %i", &nRows, &nColumns);

	matrix = (int**)malloc(nRows * sizeof(int*));

	for (int i = 0; i < nRows; i++)
	{
		matrix[i] = (int*)malloc(nColumns * sizeof(int));
	}

	fillMatrix(matrix, nRows, nColumns);
	printMatrix(matrix, nRows, nColumns);


	for (int i = 0; i < nRows; i++)
	{
		free(matrix[i]);
	}
	free(matrix);

	system("pause");
	return 0;
}