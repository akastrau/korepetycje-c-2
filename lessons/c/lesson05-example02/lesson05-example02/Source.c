#include <stdio.h>
#include <stdlib.h>

union MyUnion
{
	char* name;
	int someValue;
	double numericalValue;
};

int main()
{
	union MyUnion someUnion;
	int size = sizeof(someUnion);

	someUnion.someValue = 2;

	system("pause");
	return 0;
}