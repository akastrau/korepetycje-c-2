#include <stdio.h>
#include <stdlib.h>

typedef struct GameData
{
	int score;
	char playerName[35];
	const int playerID;
} GameData;

int main()
{
	GameData data[6] = {
		{ 12, "Stasiek", 1 },
		{ 50, "Maciek", 2 },
		{ 10, "Mateusz", 3 },
		{ 22, "Kacper", 4 }
	};

	data[5].score= 21;
	data[5].playerName = "Kuba";
	data[5].playerID = 5;
	int playersCount = sizeof(data) / sizeof(data[0]);


	FILE *file = NULL;
	errno_t error = fopen_s(&file, "gamedata.bin", "wb"); // zapis w trybie binarnym
	fwrite(&playersCount, sizeof(int), 1, file); // zapisujemy ile mamy graczy na poczatku pliku
	fwrite(data, sizeof(struct GameData), playersCount, file); // zapisujemy tablice struktur

	fclose(file);

	getch();
	return 0;
}