#pragma once
#include <stdlib.h>
#include <stdio.h>

typedef struct List
{
	int value;
	struct List *next;
} List;

List* createList(int value);
void addElement(List* list, int value);
void removeElement(List* list, int value);
void showList(const List* list);