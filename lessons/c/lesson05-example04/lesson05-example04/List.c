#include "List.h"

List * createList(int value)
{
	List* newList = (List*)malloc(sizeof(List));
	newList->next = NULL;
	newList->value = value;

	return newList;
}

void addElement(List * list, int value)
{
	List* ptr = list;
	while (ptr->next != NULL)
	{
		ptr = ptr->next;
	}

	List* newElement = (List*)malloc(sizeof(List));
	newElement->value = value;
	newElement->next = NULL;

	ptr->next = newElement;
}

void removeElement(List * list, int value)
{
	List* ptr = list;
	List* tmpPtr = list;
	
	while (ptr->next != NULL)
	{
		tmpPtr = ptr->next;
		if (tmpPtr->value == value)
		{
			if (tmpPtr->next != NULL)
			{
				ptr->next = tmpPtr->next;
			}
			else
			{
				ptr->next = NULL;
			}
			free(tmpPtr);
			return;
		}
		else
		{
			ptr = ptr->next;
		}
	}
}

void showList(const List * list)
{
	if (list == NULL)
		return;
	printf("%i ", list->value);
	while (list->next != NULL)
	{
		printf("%i ", list->next->value);
		list = list->next;
	}
	printf("\n");
}