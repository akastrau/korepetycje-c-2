#include <stdio.h>
#include <stdlib.h>

#include "List.h"

int main()
{
	List *lista = createList(2);
	addElement(lista, 3);
	addElement(lista, 7);
	showList(lista);

	removeElement(lista, 3);
	showList(lista);

	removeElement(lista, 7);
	showList(lista);

	free(lista);

	system("pause");
	return 0;
}