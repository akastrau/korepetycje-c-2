#include <stdio.h>
#include <stdlib.h>

struct struktura {
	char* name;
	int age;
};

enum Color {GREEN, YELLOW, BLACK, RED, PINK, BLUE, BROWN, WHITE, SILVER};

struct Car {
	char* brand;
	enum Color color;
};
typedef struct Car TCar;

typedef struct {
	int someValue;
	char *name;
} anotherStruct;

int main()
{
	struct struktura meineLiebe = { .name = "Wolfgang",.age = 50 };
	struct struktura keineLiebe = { "NULL", 0 };
	printf("%i\n", meineLiebe.age); 
	
	TCar car;
	car.brand = "Volvo";
	car.color = GREEN;


	anotherStruct another;
	another.someValue = 666;
	another.name = "Another struct";

	system("pause");
	return 0;
}